#include<iostream>
#include<vector>

using namespace std;

typedef struct node_structure
{
    int                   data;
    struct node_structure *next;
}node_struct;

class MyStack
{
    private:
	   node_struct *head;
    
	   node_struct* createNode(int num)
	   {
           node_struct *newNode = new node_struct();
           if(newNode)
           {
               newNode->data = num;
               newNode->next = NULL;
           }
           return newNode;
       }
    
	   bool checkIfListIsEmpty()
	   {
           return (head ? false : true);
       }
    
    public:
	   void pushNode(int num)
	   {
           node_struct *newNode = createNode(num);
           if(checkIfListIsEmpty())
           {
               head = newNode;
           }
           else
           {
               newNode->next = head;
               head          = newNode;
           }
       }
	   
	   node_struct* popNode()
	   {
           if(!checkIfListIsEmpty())
           {
               node_struct *nodeToBeReturned = head;
               head = head->next;
               return nodeToBeReturned;
           }
           return NULL;
       }
	   /*node_struct* getNodeAtTop()
        {
        return (checkIfListIsEmpty() ? NULL : head);
        }*/
};

class Engine
{
    private:
	   vector<int> arrVector;
	   MyStack     myStack;
    
    public:
	   Engine(vector<int> aV)
	   {
           arrVector = aV;
       }
    
	   void printNextGreater()
	   {
           myStack.pushNode(arrVector[0]);
           int len = (int)arrVector.size();
           for(int i = 1 ; i < len ; i++)
           {
               node_struct *currNodeAtTop = myStack.popNode();
               while(currNodeAtTop && currNodeAtTop->data < arrVector[i])
               {
                   cout<<currNodeAtTop->data<<"->"<<arrVector[i]<<endl;
                   currNodeAtTop = myStack.popNode();
               }
               if(currNodeAtTop && currNodeAtTop->data > arrVector[i])
               {
                   myStack.pushNode(currNodeAtTop->data);
               }
               myStack.pushNode(arrVector[i]);
           }
           node_struct *currNodeAtTop = NULL;
           while((currNodeAtTop = myStack.popNode()))
           {
               cout<<currNodeAtTop->data<<"->"<<"NULL"<<endl;
               currNodeAtTop = currNodeAtTop->next;
           }
       }
};

int main()
{
    int arr[] = {98, 23, 54, 12, 20, 7, 27};
    vector<int> arrVector;
    int len = (int)(sizeof(arr)/sizeof(arr[0]));
    for(int i = 0 ; i < len ; i++)
    {
        arrVector.push_back(arr[i]);
    }
    Engine e = Engine(arrVector);
    e.printNextGreater();
    return 0;
}
